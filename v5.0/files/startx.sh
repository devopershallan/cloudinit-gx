#!/bin/bash

export WINEPREFIX=/home/meta/.wine32/

if [ -d /home/meta/.arquivosgx/ex5 ]
then
  cd /home/meta/.arquivosgx/  && git fetch ; git reset -q --hard origin/master
  cd /home/meta/
  rm -rf   $WINEPREFIX"/drive_c/Program Files/MetaTrader 5 Terminal/[mM][qQ][lL]5/[eE][xX][pP][eE][rR][tT][sS]"
  mqldir=$(ls -1d $WINEPREFIX"/drive_c/Program Files/MetaTrader 5 Terminal/"[mM][qQ][lL]5)
  mkdir -p "${mqldir}/Experts"
  cp -a /home/meta/.arquivosgx/ex5/* $WINEPREFIX"/drive_c/Program Files/MetaTrader 5 Terminal/"[mM][qQ][lL]5"/Experts/"
fi

mkdir /home/meta/.ssh
openssl req -new -x509 -days 3650 -nodes -out /home/meta/.ssh/self.pem -keyout /home/meta/.ssh/self.pem -subj '/CN=metatrader/O=DevOpers/C=BR'
#(while [ 1 ]; do websockify --cert=/tmp/self.pem  6080 127.0.0.1:5900 &> /dev/null ; sleep 10s; done) &
(while [ 1 ]; do websockify --cert=/home/meta/.ssh/self.pem  6080 127.0.0.1:5900 --auth-plugin=BasicHTTPAuth --auth-source=metatrader:UUID &> /dev/null ; sleep 10s; done) &
(while [ 1 ]; do startx -- /usr/bin/Xvnc :0 -desktop metatrader -AlwaysShared -localhost -geometry 1024x768 -SecurityTypes None &> /dev/null ; sleep 10s; done) &
