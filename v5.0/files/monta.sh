#!/bin/sh

uuid=$(cat /sys/class/dmi/id/product_uuid | tr 'A-Z' 'a-z')

echo -n | openssl s_client -connect 192.168.0.11:443 | openssl x509 -text | sed -ne '/-BEGIN CERTIFICATE-/,/-END CERTIFICATE-/p' > /etc/davfs2/certs/dav.crt

/sbin/mount.davfs -o noexec,uid=meta https://192.168.0.11/by-instanceuuid/${uuid}/ /mnt

if [ -d /home/meta/.wine32/dosdevices ] && [ ! -s /home/meta/.wine32/dosdevices/d: ]
then
  ln -s /mnt/ /home/meta/.wine32/dosdevices/d\:
fi
