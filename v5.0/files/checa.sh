#!/bin/sh

PATH=/bin:/sbin:/usr/bin:/usr/sbin

# espera um tempo aleatorio de ate 1 hora
perl -e '$r=int(rand(3600)); sleep($r)' &> /dev/null

# arruma rede interna e ponto de montagem, se necessario
ifconfig eth1 | awk '$1 == "inet" {achei=1}; END {if (!achei) {print 1}}' | xargs -r ifdown eth1
ifconfig eth1 | awk '$1 == "inet" {achei=1}; END {if (!achei) {print 1}}' | xargs -r ifup eth1
mount | awk '$3 == "/mnt" {achei=1}; END {if (!achei) {print 1}}' | xargs -r /root/monta.sh

# atualiza o status baseado na busca aos logs
URLSTATUS="https://nuvem.metacloud.site/modules/servers/metatraderv2/status.php"
CURLOPT="--retry 10"

uuid=$(cat /sys/class/dmi/id/product_uuid | tr 'A-Z' 'a-z')
hostname=$(hostname -s)


pid=$(pgrep terminal.exe)
if [ -z $pid ] || [ ! -d /home/meta/.wine32/drive_c/Program\ Files/MetaTrader\ 5\ Terminal/[lL][oO][gG][sS]/ ]
then
  result="Nao conectado"
else
  result=$(
  dias=6 
  while [ $dias -ge 0 ]
  do
    iconv -f utf-16 -t utf-8 /home/meta/.wine32/drive_c/Program\ Files/MetaTrader\ 5\ Terminal/[lL][oO][gG][sS]/$(date --date="$dias day ago" +%Y%m%d).log 
    dias=$(($dias-1))
  done  | tr -d '\15\32' | egrep 'authorized|lost|authorization.*failed' | sed -E -e 's/.*Network/Network/g' -e 's/://g' -e 's/\x27//g' | awk 'BEGIN {print "Nao conectado"}; $3=="connection" && $6=="lost" {print "Nao conectado"}; $3=="authorized" {print $2" conectado em "$5}; $3=="authorization" && $6=="failed" {print $2" failed on "$5}' | tail -n 1)
fi

#echo $result
result=$(echo $result | sed -e 's/ /%20/g')
curl $CURLOPT $URLSTATUS"?hostname=$hostname&uuid=$uuid&metastatus=$result"
