#!/bin/sh

export WINEDEBUG=-all
export WINEARCH=win32
export WINEPREFIX=/home/meta/.wine32/

fbsetbg /home/meta/.fluxbox/backgrounds/fundo.png &

while [ 1 ]
do

  if [ -z $DISPLAY ]
  then
    exit
  fi

  sleep 2
  pkill -f -9 ".*\.exe"

  sleep 2
  wine c:/Program\ Files/MetaTrader\ 5\ Terminal/terminal.exe

  sleep 5
  while pgrep terminal.exe || pgrep liveupdate
  do
    sleep 5
  done

done
