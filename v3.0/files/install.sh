#!/bin/bash

export CURLOPT="--retry 10"
export URLFILES="https://ovhcloudconfig.devopers.com.br/cloudinit-gx/v3.0/files/"
#export URLSTATUS="https://metatrader.devopers.com.br/modules/servers/modulometatraderonline/status.php"
export URLSTATUS="https://metatrader.devopers.com.br/modules/servers/metatraderv2/status.php"
export URLMT5="https://download.mql5.com/cdn/web/xp.investimentos.cctvm/mt5/xpmt5setup.exe"

function error {
  curl $CURLOPT $URLSTATUS"?hostname=$hostname&uuid=$uuid&status=error"
  exit 1
}

uuid=$1
hostname=$2

export DISPLAY=:0
export WINEARCH=win32
export WINEPREFIX=/home/meta/.wine32/
export WINEDEBUG=-all

touch /tmp/instalando
cat > /tmp/timeout.sh <<EOF
#!/bin/sh
sleep 30m
if [ -f /tmp/instalando ]
then
  curl $CURLOPT $URLSTATUS"?hostname=$hostname&uuid=$uuid&status=error"
fi
EOF
chmod +x /tmp/timeout.sh
/tmp/timeout.sh &

rm -rf $WINEPREFIX
pkill -9 -f .exe
sleep 5s

curl $CURLOPT -o /home/meta/xpmt5setup.exe $URLMT5
size=$(stat -c%s /home/meta/xpmt5setup.exe)
while [ $size -lt 20000000 ]
do
  sleep 2s
  rm -f /home/meta/xpmt5setup.exe
  curl $CURLOPT -o /home/meta/xpmt5setup.exe $URLMT5
done

wine /home/meta/xpmt5setup.exe &

sleep 2s

WID=$(xdotool search --sync --name "MetaTrader 5 Terminal Setup")
while [ -z $WID ]
do
  sleep 1s
  WID=$(xdotool search --sync --name "MetaTrader 5 Terminal Setup")
  xdotool windowactivate --sync $WID
done
sleep 1s
xdotool windowactivate --sync $WID
xdotool key --clearmodifiers space
xdotool key --clearmodifiers alt+n

sleep 1s
WID=$(xdotool search --sync --name "MetaTrader 5 Terminal -")
while [ -z $WID ]
do
  sleep 1s
  WID=$(xdotool search --sync --name "MetaTrader 5 Terminal -")
  echo "Esperando MetaTrader Iniciar"
done

sleep 1s
WID=$(xdotool search --sync --name "Login")
while [ -z $WID ]
do
  sleep 1s
  WID=$(xdotool search --sync --name "Login")
  xdotool windowactivate --sync $WID
done
sleep 1s
xdotool windowactivate --sync $WID
xdotool key --clearmodifiers shift+Tab
xdotool key --clearmodifiers space

sleep 1s
xdotool key --clearmodifiers control+o
sleep 1s
WID=$(xdotool search --sync --name "Op.*es")
while [ -z $WID ]
do
  sleep 1s
  WID=$(xdotool search --sync --name "Op.*es")
  xdotool windowactivate --sync $WID
done
sleep 1s
xdotool windowactivate --sync $WID
xdotool key --repeat 7 --delay 100 Down
xdotool key Return
xdotool type "http://henrique.vilela.me"
xdotool key Return
xdotool key Down
xdotool key Return
xdotool type "https://api.telegram.org"
xdotool key Return
xdotool key Tab
xdotool key Return


WID=$(xdotool search --sync --name "MetaTrader 5 Terminal -")
while [ -z $WID ]
do
  sleep 1s
  WID=$(xdotool search --sync --name "MetaTrader 5 Terminal -")
  xdotool windowactivate --sync $WID
done
xdotool windowactivate --sync $WID
xdotool key alt+F4

curl $CURLOPT -o $WINEPREFIX/drive_c/windows/Fonts/wingding.ttf $URLFILES"/wingding.ttf"
curl $CURLOPT -o $WINEPREFIX/drive_c/windows/Fonts/webdings.ttf $URLFILES"/webdings.ttf"
if [ ! -f $WINEPREFIX/drive_c/windows/Fonts/wingding.ttf ] || [ ! -f $WINEPREFIX/drive_c/windows/Fonts/webdings.ttf ]
then
  error
fi

mkdir -p /home/meta/.ssh
curl $CURLOPT -o /home/meta/.ssh/id_rsa     $URLFILES"/id_rsa_readonly"
curl $CURLOPT -o /home/meta/.ssh/id_rsa.pub $URLFILES"/id_rsa_readonly.pub"
chmod 0600 /home/meta/.ssh/id_rsa
if [ ! -f /home/meta/.ssh/id_rsa ] || [ ! -f /home/meta/.ssh/id_rsa.pub ]
then
  error
fi

cd /home/meta/
echo "StrictHostKeyChecking no" > /home/meta/.ssh/config
chmod 0600 /home/meta/.ssh/config
git clone git@bitbucket.org:thiagodamas/arquivosgx.git
rm -rf   $WINEPREFIX"/drive_c/Program Files/MetaTrader 5 Terminal/mql5/experts"
mkdir -p $WINEPREFIX"/drive_c/Program Files/MetaTrader 5 Terminal/mql5/experts"
cp -a arquivosgx/ex5/* $WINEPREFIX"/drive_c/Program Files/MetaTrader 5 Terminal/mql5/experts/"
if [ ! -d /home/meta/arquivosgx/ex5 ]
then
  error
fi
cp -a arquivosgx/config/* $WINEPREFIX"/drive_c/Program Files/MetaTrader 5 Terminal/Config/"

curl $CURLOPT -o /home/meta/.metatrader.sh $URLFILES"/metatrader.sh"
chmod 755 /home/meta/.metatrader.sh
if [ ! -f /home/meta/.metatrader.sh ]
then
  error
fi

curl $CURLOPT -o /home/meta/.fluxbox/apps $URLFILES"/fluxbox-apps"
curl $CURLOPT -o /home/meta/.fluxbox/init $URLFILES"/fluxbox-init"
curl $CURLOPT -o /home/meta/.fluxbox/keys $URLFILES"/fluxbox-keys"
curl $CURLOPT -o /home/meta/.fluxbox/styles/Windowsbox.tgz $URLFILES"/Windowsbox.tgz"
mkdir -p /home/meta/.fluxbox/backgrounds/
curl $CURLOPT -o /home/meta/.fluxbox/backgrounds/fundo.png $URLFILES"/fundo.png"
if [ ! -f /home/meta/.fluxbox/apps ] || [ ! -f /home/meta/.fluxbox/init ] || [ ! -f /home/meta/.fluxbox/backgrounds/fundo.png ] || [ ! -f /home/meta/.fluxbox/keys ] || [ ! -f /home/meta/.fluxbox/styles/Windowsbox.tgz ]
then
  error
fi
tar -xzf /home/meta/.fluxbox/styles/Windowsbox.tgz -C /home/meta/.fluxbox/styles/
rm -f /home/meta/.fluxbox/styles/Windowsbox.tgz

crontab -l | grep -v install.sh > /tmp/crontab.meta
crontab /tmp/crontab.meta

#rm -rf /home/meta/.ssh
#rm -rf /home/meta/arquivosgx
mv /home/meta/arquivosgx /home/meta/.arquivosgx

rm /home/meta/xpmt5setup.exe

pkill fluxbox

rm -f /tmp/instalando

curl $CURLOPT $URLSTATUS"?hostname=$hostname&uuid=$uuid&status=ok"

#####

curl $CURLOPT -o /home/meta/notifica.sh $URLFILES"/notifica.sh"
chmod +x /home/meta/notifica.sh

cat >> /tmp/crontab.meta <<EOF
*/5 * * * * /home/meta/notifica.sh $hostname $uuid &> /dev/null
EOF
crontab /tmp/crontab.meta
rm -f /tmp/crontab.meta
