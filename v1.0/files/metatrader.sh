#!/bin/sh

export WINEDEBUG=-all
export WINEARCH=win32
export WINEPREFIX=/home/meta/.wine32/

while [ 1 ]; do
  sleep 2
  if [ -z DISPLAY ]; then
  pkill -f -9 ".*\.exe"
    exit
  fi
  sleep 2
  wine c:/Program\ Files/MetaTrader\ 5\ Terminal/terminal.exe
done

