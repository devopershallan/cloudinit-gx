#!/bin/bash

CURLOPT="--retry 10"
URLSTATUS="https://metatrader.devopers.com.br/modules/servers/modulometatraderonline/status.php"
export WINEPREFIX=/home/meta/.wine32/
hostname=$1
uuid=$2

nro=$(find $WINEPREFIX"/drive_c/Program Files/MetaTrader 5 Terminal/Bases" -maxdepth 1 | wc -l)

if [ $nro -gt 10 ]
then
  crontab -l | grep -v notifica.sh > /tmp/crontab.meta
  crontab /tmp/crontab.meta
  rm -f /tmp/crontab.meta
  rm -f /home/meta/notifica.sh
  curl $CURLOPT $URLSTATUS"?hostname=$hostname&uuid=$uuid&status=accessed"
fi
