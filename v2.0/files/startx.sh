#!/bin/bash

openssl req -new -x509 -days 3650 -nodes -out /tmp/self.pem -keyout /tmp/self.pem -subj '/CN=metatrader/O=DevOpers/C=BR'
#(while [ 1 ]; do websockify --cert=/tmp/self.pem  6080 127.0.0.1:5900 &> /dev/null ; sleep 10s; done) &
(while [ 1 ]; do websockify --cert=/tmp/self.pem  6080 127.0.0.1:5900 --auth-plugin=BasicHTTPAuth --auth-source=metatrader:UUID &> /dev/null ; sleep 10s; done) &
(while [ 1 ]; do startx -- /usr/bin/Xvnc :0 -desktop metatrader -AlwaysShared -localhost -geometry 1024x768 -SecurityTypes None &> /dev/null ; sleep 10s; done) &
